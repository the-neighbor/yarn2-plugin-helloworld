//HELPER FUNCTION TO PRINT OUT ALL THE INSTALLED DEPENDENCIES AND VERSION NUMBERS
//IN THE STOREDPACKAGES ATTRIBUTE OF THE PROJECT OBJECT PASSED IN AS A PARAMETER.
const printInstalledDependencies = (project) => {
    //ITERATE THROUGH THE ENTRIES IN THE STOREDPACKAGES OF THE PROJECT
    for (const [key,value] of project.storedPackages.entries()) {
	//SAVE THE KEY(SOME IDENTIFYING HASH) AND VALUE(PKG DATA)
	//OF THE CURRENT PKG DEPENDENCY TO CONSTS.
	//PRINT THE NAME AND VERSION FROM THE PKG DATA.
	console.log(`${value.name} - ${value.version}`);
    }
}

//FUNCTION TO BE RAN DURING THE VALIDATION STAGE OF A YARN COMMAND
//BEFORE INSTALL OR ADD FOR EXAMPLE ARE EXECUTED.
const validateProject = (project) => {
    //PRINT NUMBER OF DEPENDENCIES
    console.log(`START: There are ${project.storedChecksums.size} dependencies.`);
}

//FUNCTION TO BE RAN AFTER THE SUCCESSFUL COMPLETION OF YARN INSTALL
//AND FULFILLMENT OF ALL THE DEPENDENCIES FOR THE CURRENT PROJECT
const afterAllInstalled = ( project ) => {
    console.log('Everything has been installed');
    //PRINT NUMBER OF DEPENDENCIES
    console.log(`FINISH: There are ${project.storedChecksums.size} dependencies now:\n`);
    //PRINT NAME AND VERSION OF EACH INSTALLED PACKAGE IN THE PROJECT
    //USING OUR HELPER FUNCTION
    printInstalledDependencies(project);
}

//WE EXPORT THE YARN PLUGIN BY SETTING THE VALUE OF MODULE.EXPORTS
//TO AN OBJECT CONTAINING THE PLUGIN CODE AND DATA DESCRIBING THE PLUGIN
module.exports = {
    //WE USE THE NAME PROPERTY TO SPECIFY THE NAME OF OUR PLUGIN
    name: `plugin-hello-debian`,
    
    //WE SET THE FACTORY PROPERTY TO AN OBJECT
    //(OR SOME CODE THAT EVALUATES TO AN OBJECT)
    //CONTAINING ALL THE COMMANDS AND HOOKS FOR OUR PLUGIN
    
    //??I THINK WE USE REQUIRE TO EVAL AND CACHE THE RETURN OF THE CODE??
    //??BLOCK WE DESCRIBE IN THE CURLY BRACKETS, BUT AM NOT FULLY SURE??
    factory: require => {
	//OUR HELLO COMMAND WILL EXTEND AN EXISTING COMMAND CLASS
	//WE IMPORT THIS CLASS FROM CLIPANION AND SAVE IT TO A CONST
	const {Command} = require('clipanion');
	//WE CREATE OUR COMMAND AS A CLASS WHICH EXTENDS THE ONE WE JUST IMPORTED
	class HelloDebianCommand extends Command {
	    //AT THIS POINT, ALL WE PUT INSIDE THE COMMAND IS A METHOD, EXECUTE
	    //AS THE NAME SPECIFIES, THIS DEFINES THE BLOCK OF CODE TO BE EVALUATED
	    //WHENEVER THIS COMMAND IS CALLED
	    async execute () {
		this.context.stdout.write(`Hey Debian folks, this is how plugins work!\n`);
	    }
	}
	//WE SET THE PATH TO THE COMMAND
	//USING A METHOD WHICH SEEMS TO BE FROM THE PARENT CLASS(COMMAND)??
	HelloDebianCommand.addPath(`hello`);
	//WE USE THE USAGE METHOD OF OUR IMPORTED COMMAND CLASS
	//TO TURN A DICTIONARY WITH STRINGS ABOUT USAGE DATA
	//INTO A STANDARD FORMAT
	HelloDebianCommand.usage = Command.Usage({
	    description: "Says hello to Debian people",
	    details: 'This will say hello',
	    examples: [
		['Say hello', 'yarn hello']
	    ]
	})
	//WE RETURN OUR HOOKS AND COMMANDS USING AN OBJECT
	//WITH THOSE RESPECTIVE PROPERTIES OR INDICES
	return {
	    hooks: {
		validateProject,
		afterAllInstalled
	    },
	    commands: [HelloDebianCommand]
	}
    }
}
